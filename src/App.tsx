import React from 'react';
import DropdownMenu from "./components/DropdownMenu";
import {menu} from "./models/moc-menu";
import {Icon} from "./components/Icon";
import MoreVertical from "./assets/icons/more-vertical.svg"
import Menu from "./components/Menu";



export default function App() {
    // init trigger element
    const trigger = <Icon IconType={MoreVertical} />

    // just for test
    const rows = Array.from(Array(5).keys())
    const cols = Array.from(Array(5).keys())

    return (
        <div className='wrapper'>
            {rows.map((rowId) =>
                <div key={`row-${rowId}`} className='row'>
                    {cols.map((colId) =>
                        <DropdownMenu key={`col-${colId}`} trigger={trigger}>
                            <Menu menu={menu}/>
                        </DropdownMenu>
                    )}
                </div>
            )}
        </div>
  );
}
