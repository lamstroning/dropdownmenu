import React, { useState, useRef, useEffect } from 'react';
interface DropdownMenuProps {
    trigger: JSX.Element;
    children: JSX.Element;
}

//class names
const dropdownMenuClass: string = 'dropdown-menu'
const dropdownMenuContentClass: string = 'dropdown-menu__content'

export const dropdownMenuClassOpenedClass: string = 'dropdown-menu_opened'
export const dropdownMenuClassClosedClass: string = 'dropdown-menu_closed'

export const dropdownMenuContentSelector: string = `.${dropdownMenuContentClass}`
export const dropdownMenuSelector: string = `.${dropdownMenuClass}`
export const dropdownMenuOpenedSelector: string = `.${dropdownMenuClassOpenedClass}`

export default function DropdownMenu({ trigger, children }: DropdownMenuProps) {
    const [isOpen, setIsOpen] = useState<boolean>(false)
    const [menuPosition, setMenuPosition] = useState<{ top: number; left: number }>({ top: 0, left: 0 })
    const dropdownRef = useRef<HTMLDivElement>(null)

    const handleOpen = () => setIsOpen(true)
    const handleClose = () => setIsOpen(false)

    useEffect(() => {
        const handleChange = () => positionMenu()

        const handleClickOutside = (event: MouseEvent) =>
            dropdownRef.current &&
            !dropdownRef.current.contains(event.target as Node) &&
            setIsOpen(false)

        window.addEventListener('resize', handleChange)
        window.addEventListener('scroll', handleChange)
        window.addEventListener('click', handleClickOutside)

        return () => {
            window.removeEventListener('resize', handleChange)
            window.removeEventListener('scroll', handleChange)
            window.removeEventListener('click', handleClickOutside)
        }
    }, [isOpen])

    const positionMenu = () => {
        if (dropdownRef.current) {
            const content = dropdownRef.current.querySelector(dropdownMenuContentSelector)

            if (!content)
                return

            const triggerRect = dropdownRef.current.getBoundingClientRect()
            const menuRect = content.getBoundingClientRect()

            // check position
            const getTop = () => triggerRect.y - menuRect.height
            const getBottom = () => triggerRect.y + triggerRect.height
            const getLeft = () => triggerRect.x - menuRect.width + triggerRect.width
            const getRight = () => triggerRect.x

            const top = triggerRect.y > window.innerHeight / 2 ? getTop() : getBottom()
            const left = triggerRect.x > window.innerWidth / 2 ? getLeft() : getRight()

            setMenuPosition({ top: top, left: left })
        }
    }

    const handleTriggerClick = () => isOpen ? handleClose() : handleOpen()

    const handleTriggerMouseEnter = () => positionMenu()

    return (
        <div
            ref={dropdownRef}
            className={`${dropdownMenuClass} ${isOpen ? dropdownMenuClassOpenedClass : ''}`}
            onMouseEnter={handleTriggerMouseEnter}
        >
            <div className='dropdown-menu__trigger' onClick={handleTriggerClick}>
                {trigger}
            </div>
            <div
                className={dropdownMenuContentClass}
                style={{ top: menuPosition.top, left: menuPosition.left }}
            >
                {children}
            </div>
        </div>
    )
}
