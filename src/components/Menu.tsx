import React from "react";
import {Icon} from "./Icon";
import {MenuType} from "../models/models";
import {
    dropdownMenuClassClosedClass,
    dropdownMenuClassOpenedClass,
    dropdownMenuOpenedSelector,
    dropdownMenuSelector
} from "./DropdownMenu";

interface MenuProps {
    menu: MenuType[]
}

const menuClass = 'menu'
const menuItemClass = 'menu__item'
const menuTitleClass = 'menu__title'
const menuIconClass = 'menu__icon'

export default function Menu({menu}: MenuProps) {
    const onClose = (): true => {
        // get all selectors
        const allDropdownSelector = document.querySelectorAll(dropdownMenuSelector)
        const dropdownSelectorOpened = document.querySelector(dropdownMenuOpenedSelector)

        dropdownSelectorOpened?.classList.remove(dropdownMenuClassOpenedClass)
        allDropdownSelector.forEach(selector => selector.classList.add(dropdownMenuClassClosedClass))
        setTimeout(() => allDropdownSelector.forEach(selector => selector.classList.remove(dropdownMenuClassClosedClass)), 100)

        return true
    }
    const onSelect = (select) => onClose() && select()

    const renderMenuItem = ({title, icon, select}: MenuType) =>
        <button
            key={title}
            title={title}
            onClick={() => onSelect(select)}
            className={menuItemClass}
        >
            <div className={menuTitleClass}>
                {title}
            </div>
            <div className={menuIconClass}>
                <Icon IconType={icon}/>
            </div>
        </button>

    return (
        <div className={`${menuClass}`}>
            {menu.map(renderMenuItem)}
        </div>
    )
}
