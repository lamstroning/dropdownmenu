import React from "react";
import {IconType} from "../models/models";

interface IconProps {
    IconType: IconType;
    alt?: string;
    className?: string;
}

export function Icon({IconType}: IconProps)
{
    return (IconType ? <IconType /> : null)
}
