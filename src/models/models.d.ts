import {IconType} from "./icon";

//we can import icon from '../src/assets/icons'
export type IconType = JSX.Element<unknown>

export type MenuType ={
    title: string,
    icon: IconType,
    select: () => void
}
