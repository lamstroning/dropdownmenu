import {MenuType} from './models';
import ShareIcon from '../assets/icons/share.svg'
import EditIcon from '../assets/icons/edit.svg'
import TrashIcon from '../assets/icons/trash.svg'

// test object
export const menu: MenuType[] = [
    {
        title: 'Share link',
        select: () => console.log('Shared!'),
        icon: ShareIcon
    },
    {
        title: 'Edit',
        select: () => console.log('trigger edit'),
        icon: EditIcon
    },
    {
        title: 'Delete',
        select: () => console.log('trigger delete'),
        icon: TrashIcon
    },
    {
        title: 'Overflow test Overflow test Overflow test Overflow test',
        select: () => console.log('trigger overflow'),
        icon: ShareIcon
    },
]
